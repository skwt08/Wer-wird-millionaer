#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h"
#include <string.h>
#include "FrageS.h"

Frage a0, a1, a2, a3, b0, b1, b2, b3, c0, c1, c2, c3, d0, d1, d2, d3, e0, e1, e2, e3, f0, f1, f2, f3, g0, g1, g2, g3, h0, h1, h2, h3, i0, i1, i2, i3, j0, j1, j2, j3, k0, k1, k2, k3, l0, l1, l2, l3, m0, m1, m2, m3,
n0, n1, n2, n3, o0, o1, o2, o3;


void Fragensammlung() {

	// Frage a0
	strcpy_s(a0.f, "Was kann man essen?");
	strcpy_s(a0.a, "Aktenkoffer");
	strcpy_s(a0.b, "Schulranzen");
	strcpy_s(a0.c, "Rucksack");
	strcpy_s(a0.d, "Apfeltasche");
	a0.ant = 'D';

	// Frage a1
	strcpy_s(a1.f, "Wobei handelt es sich nicht um ein Reptil?");
	strcpy_s(a1.a, "Wiesenotter");
	strcpy_s(a1.b, "Kreuzotter");
	strcpy_s(a1.c, "Puffotter");
	strcpy_s(a1.d, "Eidotter");
	a1.ant = 'D';

	// Frage a2
	strcpy_s(a2.f, "Peter Kreuder komponierte 1936 den Schlager 'Ich wollt' ich waer' ein ...'?");
	strcpy_s(a2.a, "Hund");
	strcpy_s(a2.b, "Huhn");
	strcpy_s(a2.c, "Hummer");
	strcpy_s(a2.d, "Huflattich");
	a2.ant = 'B';

	// Frage a3
	strcpy_s(a3.f, "Wie nennt man grossen Wirbel um eine beruehmte Persoenlichkeit?");
	strcpy_s(a3.a, "Prominentenkirmes");
	strcpy_s(a3.b, "Starrummel");
	strcpy_s(a3.c, "VIP-Jahrmarkt");
	strcpy_s(a3.d, "Goetterzirkus");
	a3.ant = 'B';



	// Fragestufe 2


	// Frage b0
	strcpy_s(b0.f, "Kleine Suenden straft der liebe Gott dem Volksmund nach ...?");
	strcpy_s(b0.a, "sofort");
	strcpy_s(b0.b, "so gut wie nie");
	strcpy_s(b0.c, "mit Fernsehverbot");
	strcpy_s(b0.d, "in neun Monaten");
	b0.ant = 'A';

	// Frage b1
	strcpy_s(b1.f, "Wobei handelt es sich um ein Notsignal im internationalen Funkverkehr?");
	strcpy_s(b1.a, "Mayday");
	strcpy_s(b1.b, "Down Town");
	strcpy_s(b1.c, "Jetset");
	strcpy_s(b1.d, "Flower Power");
	b1.ant = 'A';

	// Frage b2
	strcpy_s(b2.f, "Welches Imitat erfuellt sichtlich seinen Zweck?");
	strcpy_s(b2.a, "Tomatenroeter");
	strcpy_s(b2.b, "Kaffeeweisser");
	strcpy_s(b2.c, "Senfgelber");
	strcpy_s(b2.d, "Schokoladenbraeuner");
	b2.ant = 'B';

	// Frage b3
	strcpy_s(b3.f, "Was steht ueber dem Haupteingang des Reichstagsgebaeudes in Berlin?");
	strcpy_s(b3.a, "Dem deutschen Volke");
	strcpy_s(b3.b, "Achtung, Bundestag!");
	strcpy_s(b3.c, "Nur fuer Abgeordnete");
	strcpy_s(b3.d, "Bitte nicht stoeren!");
	b3.ant = 'A';


	// Fragestufe 3

	// Frage c0
	strcpy_s(c0.f, "Wer davonlaeuft, der gibt einer Redewendung nach ...?");
	strcpy_s(c0.a, "Wechselgeld");
	strcpy_s(c0.b, "Fersengeld");
	strcpy_s(c0.c, "Kleingeld");
	strcpy_s(c0.d, "Kindergeld");
	c0.ant = 'B';

	// Frage c1
	strcpy_s(c1.f, "Eine populaere Spielart der amerikanischen Volksmusik heisst Country & ...?");
	strcpy_s(c1.a, "Thriller");
	strcpy_s(c1.b, "Comedy");
	strcpy_s(c1.c, "Slapstick");
	strcpy_s(c1.d, "Western");
	c1.ant = 'D';

	// Frage c2
	strcpy_s(c2.f, "Wie nennt man die direkte Verbindung von Plus- und Minuspol einer Spannungsquelle?");
	strcpy_s(c2.a, "Glasbruch");
	strcpy_s(c2.b, "Wasserschaden");
	strcpy_s(c2.c, "Rohrverstopfung");
	strcpy_s(c2.d, "Kurzschluss");
	c2.ant = 'D';

	// Frage c3
	strcpy_s(c3.f, "Wie heisst - laut einem Maerchen der Brueder Grimm - die Schwester von Schneeweisschen?");
	strcpy_s(c3.a, "Fliederlila");
	strcpy_s(c3.b, "Maisgelb");
	strcpy_s(c3.c, "Rosenrot");
	strcpy_s(c3.d, "Kornblumenblau");
	c3.ant = 'C';

	// Fragestufe 4


	// Frage d0
	strcpy_s(d0.f, "Wo befindet sich das Orchester waehrend einer Opernauffuehrung?");
	strcpy_s(d0.a, "Fahrradweg");
	strcpy_s(d0.b, "Gruenstreifen");
	strcpy_s(d0.c, "Boeschung");
	strcpy_s(d0.d, "Graben");
	d0.ant = 'D';

	// Frage d1
	strcpy_s(d1.f, "Nicht nur Ross und Reiter brauchen zum Sieg oft ein gutes ...?");
	strcpy_s(d1.a, "Shwedish");
	strcpy_s(d1.b, "Denish");
	strcpy_s(d1.c, "Finish");
	strcpy_s(d1.d, "Norvegish");
	d1.ant = 'C';

	// Frage d2
	strcpy_s(d2.f, "Vor welchen gesetzlosen Gesellen fuerchteten sich schon die alten Rittersleut'?");
	strcpy_s(d2.a, "Gebueschganoven");
	strcpy_s(d2.b, "Heckenstrolche");
	strcpy_s(d2.c, "Baumbanditen");
	strcpy_s(d2.d, "Strauchdiebe");
	d2.ant = 'D';

	// Frage d3
	strcpy_s(d3.f, "Durch welches Verfahren schickte man im alten Athen seine Mitbuerger in die Verbannung?");
	strcpy_s(d3.a, "Goetterspeise");
	strcpy_s(d3.b, "Henkersmahlzeit");
	strcpy_s(d3.c, "Scherbengericht");
	strcpy_s(d3.d, "Grillteller");
	d3.ant = 'C';




	// Fragestufe 5



	// Frage e0
	strcpy_s(e0.f, "Wie nennt man ein gedrucktes Rundschreiben des Papstes?");
	strcpy_s(e0.a, "Enzephalitis");
	strcpy_s(e0.b, "Enzym");
	strcpy_s(e0.c, "Enzyklopaedie");
	strcpy_s(e0.d, "Enzyklika");
	e0.ant = 'D';

	// Frage e1
	strcpy_s(e1.f, "Fuer Kosmetika und als Nahrungsergaenzungsmittel verwendet man das oel der ...?");
	strcpy_s(e1.a, "Wunderkerze");
	strcpy_s(e1.b, "Nachtkerze");
	strcpy_s(e1.c, "Taufkerze");
	strcpy_s(e1.d, "Zuendkerze");
	e1.ant = 'B';

	// Frage e2
	strcpy_s(e2.f, "Was wird nicht durch Reflexe ausgeloest?");
	strcpy_s(e2.a, "Gaehnen");
	strcpy_s(e2.b, "Niesen");
	strcpy_s(e2.c, "Kauen");
	strcpy_s(e2.d, "Schlucken");
	e2.ant = 'C';

	// Frage e3
	strcpy_s(e3.f, "Was ist die Amtssprache von Mexiko?");
	strcpy_s(e3.a, "Portugiesisch");
	strcpy_s(e3.b, "Englisch");
	strcpy_s(e3.c, "Franzoesisch");
	strcpy_s(e3.d, "Spanisch");
	e3.ant = 'D';


	// Fragestufe 6

	// Frage f0
	strcpy_s(f0.f, "Mit den Worten 'Der Weltraum, unendliche Weiten' beginnen die Abenteuer des Raumschiffes ...?");
	strcpy_s(f0.a, "Galactica");
	strcpy_s(f0.b, "Orion");
	strcpy_s(f0.c, "Nostromo");
	strcpy_s(f0.d, "Enterprise");
	f0.ant = 'D';

	// Frage f1
	strcpy_s(f1.f, "Welches Ministerium wird umgangssprachlich als Hardthoehe bezeichnet?");
	strcpy_s(f1.a, "Justiz");
	strcpy_s(f1.b, "Finanzen");
	strcpy_s(f1.c, "Verteidigung");
	strcpy_s(f1.d, "Gesundheit");
	f1.ant = 'C';

	// Frage f2
	strcpy_s(f2.f, "Unter welchem Namen sangen Wigald Boning und Olli Dittrich 'Lieder, die die Welt nicht braucht'?");
	strcpy_s(f2.a, "Die Behaemmerten");
	strcpy_s(f2.b, "Die Abgedrehten");
	strcpy_s(f2.c, "Die Doofen");
	strcpy_s(f2.d, "Die Irren");
	f2.ant = 'C';

	// Frage f3
	strcpy_s(f3.f, "Der potenzielle Bundespraesident (2004) Horst Koehler war davor Chef des ...?");
	strcpy_s(f3.a, "IWF");
	strcpy_s(f3.b, "DRK");
	strcpy_s(f3.c, "IOC");
	strcpy_s(f3.d, "DFB");
	f3.ant = 'A';


	// Fragestufe 7

	// Frage g0
	strcpy_s(g0.f, "Was ist ein Rebus?");
	strcpy_s(g0.a, "Kreditinstitut");
	strcpy_s(g0.b, "Werkzeug");
	strcpy_s(g0.c, "Weinstock");
	strcpy_s(g0.d, "Bilderraetsel");
	g0.ant = 'D';

	// Frage g1
	strcpy_s(g1.f, "Was kann man zum Loesen festgerosteter Schrauben verwenden?");
	strcpy_s(g1.a, "Kriechoel");
	strcpy_s(g1.b, "Schleichkerosin");
	strcpy_s(g1.c, "Troedelbenzin");
	strcpy_s(g1.d, "Bummeldiesel");
	g1.ant = 'A';

	// Frage g2
	strcpy_s(g2.f, "Wer gehoert nicht zu Dorothys Begleitern in 'Der Zauberer von Oz'?");
	strcpy_s(g2.a, "fast kopfloser Nick");
	strcpy_s(g2.b, "Vogelscheuche");
	strcpy_s(g2.c, "Zinnmann");
	strcpy_s(g2.d, "aengstlicher Loewe");
	g2.ant = 'A';

	// Frage g3
	strcpy_s(g3.f, "Womit bescherte Songwriter Leonard Cohen der Welt einen Klassiker?");
	strcpy_s(g3.a, "Suzanne");
	strcpy_s(g3.b, "Michelle");
	strcpy_s(g3.c, "Rosanna");
	strcpy_s(g3.d, "Lucille");
	g3.ant = 'A';





	// Fragestufe 8



	// Frage h0
	strcpy_s(h0.f, "Wie heisst ein Kanton der Schweiz?");
	strcpy_s(h0.a, "Baden-Baden");
	strcpy_s(h0.b, "Karlsruhe");
	strcpy_s(h0.c, "Konstanz");
	strcpy_s(h0.d, "Freiburg");
	h0.ant = 'D';

	// Frage h1
	strcpy_s(h1.f, "Ein Stummfilmklassiker von Sergej Eisenstein heisst 'Panzerkreuzer ...'?");
	strcpy_s(h1.a, "Rasputin");
	strcpy_s(h1.b, "Potemkin");
	strcpy_s(h1.c, "Iljuschin");
	strcpy_s(h1.d, "Putin");
	h1.ant = 'B';

	// Frage h2
	strcpy_s(h2.f, "Wie nennt man von Gletschern transportierten Gesteinsschutt?");
	strcpy_s(h2.a, "Moraene");
	strcpy_s(h2.b, "Muraene");
	strcpy_s(h2.c, "Morelle");
	strcpy_s(h2.d, "Murnau");
	h2.ant = 'A';

	// Frage h3
	strcpy_s(h3.f, "Wer schoss in 62 Fussball-Laenderspielen 68 Tore?");
	strcpy_s(h3.a, "Gerd Mueller");
	strcpy_s(h3.b, "Horst Hrubesch");
	strcpy_s(h3.c, "Helmut Rahn");
	strcpy_s(h3.d, "Jupp Heynckes");
	h3.ant = 'A';



	// Fragestufe 9

	// Frage i0
	strcpy_s(i0.f, "Welches deutsche Fuerstenhaus organisierte bis ins 19. Jahrhundert die kaiserliche Reichspost?");
	strcpy_s(i0.a, "Fuerstenberg");
	strcpy_s(i0.b, "Schaumburg-Lippe");
	strcpy_s(i0.c, "Hohenlohe");
	strcpy_s(i0.d, "Thurn und Taxis");
	i0.ant = 'D';

	// Frage i1
	strcpy_s(i1.f, "Bei den Olympischen Spielen 1900 gab es einen Wettbewerb im Schiessen auf lebende ...?");
	strcpy_s(i1.a, "Hasen");
	strcpy_s(i1.b, "Ratten");
	strcpy_s(i1.c, "Fasane");
	strcpy_s(i1.d, "Tauben");
	i1.ant = 'D';

	// Frage i2
	strcpy_s(i2.f, "Was ist das so genannte Horn von Afrika?");
	strcpy_s(i2.a, "Sternbild");
	strcpy_s(i2.b, "Halbinsel");
	strcpy_s(i2.c, "fossiles Kultobjekt");
	strcpy_s(i2.d, "Berg in Tansania");
	i2.ant = 'B';

	// Frage i3
	strcpy_s(i3.f, "Wenn ueber die Sangesversuche von K-Fed berichtet wird, geht es um den Ehemann von ...?");
	strcpy_s(i3.a, "Jennifer Lopez");
	strcpy_s(i3.b, "Beyonc� Knowles");
	strcpy_s(i3.c, "Christina Aguilera");
	strcpy_s(i3.d, "Britney Spears");
	i3.ant = 'D';



	// Fragestufe 10



	// Frage j0
	strcpy_s(j0.f, "Wie heisst eine Klasse der Wirbeltiere mit mehr als 20.000 bekannten Arten?");
	strcpy_s(j0.a, "Schuppenfische");
	strcpy_s(j0.b, "Kiemenfische");
	strcpy_s(j0.c, "Knochenfische");
	strcpy_s(j0.d, "Flossenfische");
	j0.ant = 'C';

	// Frage j1
	strcpy_s(j1.f, "Wo machte man Mitte des 20. Jh. einen fuer das Bibelverstaendnis spektakulaeren Schriftrollen-Fund?");
	strcpy_s(j1.a, "Qumran");
	strcpy_s(j1.b, "Chich�n Itz�");
	strcpy_s(j1.c, "Angkor Vat");
	strcpy_s(j1.d, "Herculaneum");
	j1.ant = 'A';

	// Frage j2
	strcpy_s(j2.f, "Wohin muss man reisen, wenn man die ueberreste der antiken Metropole Pergamon sehen will?");
	strcpy_s(j2.a, "Tuerkei");
	strcpy_s(j2.b, "aegypten");
	strcpy_s(j2.c, "Zypern");
	strcpy_s(j2.d, "Kreta");
	j2.ant = 'A';

	// Frage j3
	strcpy_s(j3.f, "Wobei handelt es sich nicht um ein Insekt?");
	strcpy_s(j3.a, "Stechmuecke");
	strcpy_s(j3.b, "Wintermuecke");
	strcpy_s(j3.c, "Grasmuecke");
	strcpy_s(j3.d, "Kriebelmuecke");
	j3.ant = 'C';


	// Fragestufe 11


	// Frage k0
	strcpy_s(k0.f, "Wie heisst das traditionelle Pferderennen in Siena?");
	strcpy_s(k0.a, "Calcio");
	strcpy_s(k0.b, "Palio");
	strcpy_s(k0.c, "Barolo");
	strcpy_s(k0.d, "Boccia");
	k0.ant = 'B';

	// Frage k1
	strcpy_s(k1.f, "Wie nennen Mathematiker einen Grundsatz, der nicht von anderen Saetzen abgeleitet werden kann?");
	strcpy_s(k1.a, "Idiom");
	strcpy_s(k1.b, "Axiom");
	strcpy_s(k1.c, "Algorithmus");
	strcpy_s(k1.d, "Hypothese");
	k1.ant = 'B';

	// Frage k2
	strcpy_s(k2.f, "Wer war nie Kaiser des Heiligen Roemischen Reiches");
	strcpy_s(k2.a, "Ludwig der Bayer");
	strcpy_s(k2.b, "Heinrich der Loewe");
	strcpy_s(k2.c, "Otto der Grosse");
	strcpy_s(k2.d, "Friedrich Barbarossa");
	k2.ant = 'B';

	// Frage k3
	strcpy_s(k3.f, "Nach einem Stadtteil welcher Metropole benannte sich der Regisseur Rosa von Praunheim?");
	strcpy_s(k3.a, "Magdeburg");
	strcpy_s(k3.b, "Frankfurt am Main");
	strcpy_s(k3.c, "Hamburg");
	strcpy_s(k3.d, "Koeln");
	k3.ant = 'B';


	// Fragestufe 12


	// Frage l0
	strcpy_s(l0.f, "Was ist nach Mahon, dem Hauptort der Insel Menorca, benannt?");
	strcpy_s(l0.a, "Marone");
	strcpy_s(l0.b, "Mahagoni");
	strcpy_s(l0.c, "Marihuana");
	strcpy_s(l0.d, "Mayonnaise");
	l0.ant = 'D';

	// Frage l1
	strcpy_s(l1.f, "Tabak zaehlt zur selben Pflanzenfamilie wie die ...?");
	strcpy_s(l1.a, "Kartoffel");
	strcpy_s(l1.b, "Karotte");
	strcpy_s(l1.c, "Kamille");
	strcpy_s(l1.d, "Kastanie");
	l1.ant = 'A';

	// Frage l2
	strcpy_s(l2.f, "Welcher Schriftsteller verfasste 'Die New York - Trilogie'?");
	strcpy_s(l2.a, "John Irving");
	strcpy_s(l2.b, "Paul Auster");
	strcpy_s(l2.c, "Dan Brown");
	strcpy_s(l2.d, "Ernest Hemmingway");
	l2.ant = 'B';

	// Frage l3
	strcpy_s(l3.f, "Wessen Romane waeren nie veroeffentlicht worden, wenn man seinem letzten Willen entsprochen haette?");
	strcpy_s(l3.a, "Hermann Hesse");
	strcpy_s(l3.b, "Theodor Fontane");
	strcpy_s(l3.c, "Franz Kafka");
	strcpy_s(l3.d, "Robert Musil");
	l3.ant = 'C';



	// Fragestufe 13


	// Frage m0
	strcpy_s(m0.f, "Welcher deutsche Boxer schlug im Juni 1952 den Ringrichter Max Pippow zu Boden?");
	strcpy_s(m0.a, "Peter Mueller");
	strcpy_s(m0.b, "Eckhard Dagge");
	strcpy_s(m0.c, "Bubi Scholz");
	strcpy_s(m0.d, "Max Schmeling");
	m0.ant = 'A';

	// Frage m1
	strcpy_s(m1.f, "Was sieht aus wie ein Kolibri, ist aber ein Schmetterling?");
	strcpy_s(m1.a, "Taubenschwaenzchen");
	strcpy_s(m1.b, "Meisenfluegelchen");
	strcpy_s(m1.c, "Amselschnaebelchen");
	strcpy_s(m1.d, "Gaensefederchen");
	m1.ant = 'A';

	// Frage m2
	strcpy_s(m2.f, "Am Grund tiefer Seen ist das Wasser ...?");
	strcpy_s(m2.a, "nie kaelter als 4�C");
	strcpy_s(m2.b, "sauerstofffrei");
	strcpy_s(m2.c, "nicht trinkbar");
	strcpy_s(m2.d, "lichtundurchlaessig");
	m3.ant = 'A';

	// Frage m3
	strcpy_s(m3.f, "Wo wohnt Astrid Lindgrens Titelheldin Lotta?");
	strcpy_s(m3.a, "Hoellenlaermgasse");
	strcpy_s(m3.b, "Krachmacherstrasse");
	strcpy_s(m3.c, "Donnerschlagallee");
	strcpy_s(m3.d, "Radaubruderweg");
	m3.ant = 'B';



	// Fragestufe 14


	// Frage n0
	strcpy_s(n0.f, "Gegen wen setzte sich Bonn 1949 bei der Wahl zur Bundeshauptstadt mit 33:29 Stimmen durch?");
	strcpy_s(n0.a, "Kassel");
	strcpy_s(n0.b, "Muenchen");
	strcpy_s(n0.c, "Frankfurt am Main");
	strcpy_s(n0.d, "Westberlin");
	n0.ant = 'C';

	// Frage n1
	strcpy_s(n1.f, "Welche Weltreligion kennt keine Schutzengel?");
	strcpy_s(n1.a, "Judentum");
	strcpy_s(n1.b, "Islam");
	strcpy_s(n1.c, "Christentum");
	strcpy_s(n1.d, "Buddhismus");
	n1.ant = 'D';

	// Frage n2
	strcpy_s(n2.f, "Giraffe, Pfau und Eidechse sind ...?");
	strcpy_s(n2.a, "lebend gebaerend");
	strcpy_s(n2.b, "heilige Tiere in Indien");
	strcpy_s(n2.c, "Tischler-Werkzeuge");
	strcpy_s(n2.d, "Sternbilder");
	n2.ant = 'D';

	// Frage n3
	strcpy_s(n3.f, "Wo befindet sich der Hauptsitz der UNESCO?");
	strcpy_s(n3.a, "Bruessel");
	strcpy_s(n3.b, "Paris");
	strcpy_s(n3.c, "London");
	strcpy_s(n3.d, "Helsinki");
	n3.ant = 'B';


	// Fragestufe 15



	// Frage o0
	strcpy_s(o0.f, "Welche beiden Gibb-Brueder der Popband The Bee Gees sind Zwillinge?");
	strcpy_s(o0.a, "Robin und Barry");
	strcpy_s(o0.b, "Maurice und Robin");
	strcpy_s(o0.c, "Barry und Maurice");
	strcpy_s(o0.d, "Andy und Robin");
	o0.ant = 'B';

	// Frage o1
	strcpy_s(o1.f, "Wer bekam 1954 den Chemie- und 1962 den Friedensnobelpreis?");
	strcpy_s(o1.a, "Linus Pauling");
	strcpy_s(o1.b, "Otto Hahn");
	strcpy_s(o1.c, "Pearl S Buck");
	strcpy_s(o1.d, "Albert Schweitzer");
	o1.ant = 'A';

	// Frage o2
	strcpy_s(o2.f, "Mit wem stand Edmund Hillary 1953 auf dem Gipfel des Mount Everest?");
	strcpy_s(o2.a, "Nasreddin Hodscha");
	strcpy_s(o2.b, "Nursay Pimsorn");
	strcpy_s(o2.c, "Tenzing Norgay");
	strcpy_s(o2.d, "Abrindranath Singh");
	o2.ant = 'C';

	// Frage o3
	strcpy_s(o2.f, "Welcher beruehmte Schriftsteller erbaute als diplomierter Architekt ein Freibad in Zuerich?");
	strcpy_s(o2.a, "Joseph Roth");
	strcpy_s(o2.b, "Martin Walser");
	strcpy_s(o2.c, "Max Frisch");
	strcpy_s(o2.d, "Friedrich Duerrenmatt");
	o2.ant = 'C';

};