// WWM.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stdafx.h"
#include "FrageS.h"		
#include <time.h>

void Fragensammlung();				// initialiesiert die fragen

extern Frage a0, a1, a2, a3, b0, b1, b2, b3, c0, c1, c2, c3, d0, d1, d2, d3, e0, e1, e2, e3, f0, f1, f2, f3, g0, g1, g2, g3, h0, h1, h2, h3, i0, i1, i2, i3, j0, j1, j2, j3, k0, k1, k2, k3, l0, l1, l2, l3, m0, m1, m2, m3,
n0, n1, n2, n3, o0, o1, o2, o3;

char stufen[][14] = { "50 EUR", "100 EUR", "200 EUR", "300 EUR", "500 EUR", "1.000 EUR", "2.000 EUR", "4.000 EUR", "8.000 EUR", "16.000 EUR", "32.000 EUR", "64.000 EUR", "125.000 EUR", "500.000 EUR", "1.000.000 EUR" };  // gewinnstufen
char eingabe;
char eingabeJ;
int level = 0;							// aktuelle Spielstufe
int gameover = 0;						// wenn 1 wird -> spiel vorbei
int jHalbverfuegbar = 1;				// verf�gbarkeit der Joker
int jPubverfuegbar = 1;					// verf�gbarkeit der Joker
Frage momF;								// momentane Frage

int checkAnt();
void ausgabe(Frage frage);
void jokerHalb();



void jokerHalb() {								// 50/50 joker
	if (momF.ant == 'A') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)%20s\t\t\t  (B)\n", momF.a);
		printf("(C)\t\t\t%45s  (D)\n", momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jHalbverfuegbar = 0;
	}
	else if (momF.ant == 'B') {
		system("cls");
		printf("\n\nFragestufe:%i\n", level);
		printf("%s\n", momF.f);
		printf("(A)%20s\t\t\t%45s  ()B\n", momF.a, momF.b);
		printf("(C)\t\t\t   (D)\n");
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jHalbverfuegbar = 0;
	}
	else if (momF.ant == 'C') {
		system("cls");
		printf("\n\nFragestufe:%i\n", level);
		printf("%s\n", momF.f);
		printf("(A)\t\t\t%45s(B)\n", momF.b);
		printf("(C)%20s\t\t\t  (D)\n", momF.c);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jHalbverfuegbar = 0;
	}
	else if (momF.ant == 'D') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)\t\t\t%45s  (B)\n", momF.b);
		printf("(C)\t\t\t%45s  (D)\n", momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jHalbverfuegbar = 0;
	}
}	



void jokerPub() {						// Publikums Joker
	if (momF.ant == 'A') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)%20s  73%%\t\t\t%45s  9%%  (B)\n", momF.a, momF.b);
		printf("(C)%20s  5%%\t\t\t%45s  13%%  (D)\n", momF.c, momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jPubverfuegbar = 0;
	}
	else if (momF.ant == 'B') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)%20s  7%%\t\t\t%45s 82%%  (B)\n", momF.a, momF.b);
		printf("(C)%20s  3%%\t\t\t%45s  8%%  (D)\n", momF.c, momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jPubverfuegbar = 0;
	}
	else if (momF.ant == 'C') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)%20s  6%%\t\t\t%45s  9%%  (B)\n", momF.a, momF.b);
		printf("(C)%20s  77%%\t\t\t%45s  8%%  (D)\n", momF.c, momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jPubverfuegbar = 0;
	}
	else if (momF.ant == 'D') {
		system("cls");
		printf("Fragestufe:%i\n", level);
		printf("\n\n%s\n", momF.f);
		printf("(A)%20s  10%%\t\t\t%45s  6%%  (B)\n", momF.a, momF.b);
		printf("(C)%20s  9%%\t\t\t%45s  75%%  (D)\n", momF.c, momF.d);
		printf("Bitte geben sie ihre Antwort ein:");
		scanf("%c", &eingabe);
		getchar();
		checkAnt();
		jPubverfuegbar = 0;
	}
}		

void ausgabeGameOver() {
	system("cls");
	printf("Antwort %c ist falsch!\n", eingabe);
	printf("Game over.\n");
	getchar();
}


int checkAnt() {						// �berpruft die eingabe/antworten auf die fragen (auch ob der einsatz von Jokern gew�nscht ist
	char richtigeAnt = momF.ant;
	
		if (eingabe == richtigeAnt) {
			printf("Antwort %c ist korrekt!\n\n", momF.ant);
			getchar();
			return 1;
		}
		else if (eingabe == 'H' && jHalbverfuegbar == 1) {
			jokerHalb();
		}
		else if (eingabe == 'H' && jHalbverfuegbar == 0) {
			system("cls");
			printf("Dieser Joker steht ihnen nichtmehr zur verfuegung.");
			getchar();
			ausgabe(momF);
			checkAnt();
		}
		else if (eingabe == 'P' && jPubverfuegbar == 1) {
			jokerPub();
		}
		else if (eingabe == 'H' && jPubverfuegbar == 0) {
			printf("Dieser Joker steht ihnen nichtmehr zur verfuegung.");
			ausgabe(momF);
			checkAnt();
		}
		else {
			ausgabeGameOver();
			gameover = 1;
			return 0;
		};
	}


void ausgabe(Frage frage) {					// gibt die frage aus
	system("cls");
	printf("Gewinnstufe:%s\n", stufen[level]);
	printf("\n\n%s\n\n", frage.f);
	printf("(A)%20s\t\t\t%45s  (B)\n", frage.a, frage.b);
	printf("(C)%20s\t\t\t%45s  (D)\n", frage.c, frage.d);
	printf("\nBitte geben sie ihre Antwort ein:");
	scanf("%c", &eingabe);
	getchar();

}

void hallo() {					// begr��ung vor beginn des Spieldurchlaufes
	printf("Herzlich Willkommen bei Wer wird Millionaer!\n\nDie Spielregeln sind wie folgt:\nGeben sie ihre Antwort auf die Frage bitte mit dem enstprechenden Buchstaben *gross geschrieben*\nund bestaetigen sie dann.\n\nSie haben Zwei Joker zur verfuegung, einen 50/50- und einen Publikums- Joker.\nDiese koennen sie verwenden indem sie 'P' oder 'H' als ihre Antwort eingeben.");
	printf("\n\nViel Spass beim raten!");
	getchar();

}

void randomF(int level) {				// w�hlt je nach Spielstufe eine von 4 Fragen random(mit zeit) aus

	Frage frage[15][4] = {
		{ a0, a1, a2, a3, } ,
		{ b0, b1, b2, b3, } ,
		{ c0, c1, c2, c3, } ,
		{ d0, d1, d2, d3, } ,
		{ e0, e1, e2, e3, } ,
		{ f0, f1, f2, f3, } ,
		{ g0, g1, g2, g3, } ,
		{ h0, h1, h2, h3, } ,
		{ i0, i1, i2, i3, } ,
		{ j0, j1, j2, j3, } ,
		{ k0, k1, k2, k3, } ,
		{ l0, l1, l2, l3, } ,
		{ m0, m1, m2, m3, } ,
		{ n0, n1, n2, n3, } ,
		{ o0, o1, o2, o3, } ,
	};
	srand(time(NULL));
	momF = frage[level][rand() % 4];				// Zuf�llige Frage der Aktuellen Spielstufe								
	ausgabe(momF);								// Frage und Antwort-m�glichkeiten Ausgeben
}



int main()
{
	Fragensammlung();
	hallo();										// begr��ung und erkl�ren der regeln
	randomF(level);									// Vorher einmal mit '0' ausgef�hrt da sonst nicht bei 0 beginnt
	do {
		if (checkAnt()) {		// �berprfung ob Spiel gewonnen ist
			if (level == 14) {
				printf("Spiel Gewonnen");
				break;
			}
			randomF(++level);
		}
	} while (gameover != 1);					// beendet das Spiel
	getchar();
	return 0;
}

